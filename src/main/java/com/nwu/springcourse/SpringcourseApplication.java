package com.nwu.springcourse;

import com.nwu.springcourse.beans.Greeting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SpringcourseApplication implements CommandLineRunner {

    @Autowired
    private Greeting greeting;

	public static void main(String[] args) {
		SpringApplication.run(SpringcourseApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        log.info("running spring boot command line app");

        greeting.sayHello();
    }
}

