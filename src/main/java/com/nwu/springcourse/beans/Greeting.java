package com.nwu.springcourse.beans;

import org.springframework.stereotype.Component;

@Component
public class Greeting {

    private static String GREETING_MESSAGE = "Hello NWU Spring folks!!";

    public void sayHello() {
        System.out.println(GREETING_MESSAGE);
    }
}
